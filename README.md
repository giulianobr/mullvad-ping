# Mullvad VPN Ping

## Objective

The `mullvad-ping` project is a simple utility designed to check the latency of VPN servers provided by Mullvad. It aims to help users select the optimal server for their needs by measuring the response time of various servers.

## Usage

To run this project using the Go CLI, please follow the steps below:

1. **Install Go**: Make sure that Go is installed on your system. If it is not already installed, you can download it from [here](https://golang.org/dl/).

2. **Clone the Repository**: Clone this repository to your local machine using Git.
    ```sh
    git clone https://gitlab.com/giulianobr/mullvad-ping.git
    cd mullvad-ping
    ```

3. **Build the Project**: Compile the project using the Go CLI.
    ```sh
    go build
    ```

4. **Run the Project**: Execute the binary to start the ping utility.
    ```sh
    ./mullvad-ping
    ```
